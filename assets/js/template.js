$(function () {
  //    $('.grid .col').shuffle();
  $(".button-collapse").sideNav();

  $('.grid').masonry({
    itemSelector: '.col',
    columnWidth: '.grid-sizer',
    percentPosition: true,
    originLeft: true
  });

  $('#tabs_filter a').click(function () {
    var origin = $(this);
    var filter_type = origin.text().toLowerCase();
    if (filter_type == 'all') {
      var all_items = $('.grid .col');
      all_items.show();
    } else {
      var show_items = $('.grid .col.' + filter_type);
      var hide_items = $('.grid .col:not(.' + filter_type + ')');
      hide_items.hide();
      show_items.show();
    }

    $('.grid').masonry('layout');
  });

  $('.nav-content.pushpin').pushpin({
    top: $('.nav-content').offset().top
  });

  $('.card-image').materialbox();
  //    $('img').lazyLoad();
  // Make sure everything is positioned correctly
  setTimeout(function () {
    $('.grid').masonry('layout');
  }, 4000);

  var options = [
    {
      selector: '.grid .col',
      offset: 600,
      callback: function (el) {
        $(el).removeClass('inactive');
        console.log('fire');
      }
    }
  ];

  Materialize.scrollFire(options);

});
