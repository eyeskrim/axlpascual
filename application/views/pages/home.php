<div class="nav-content pushpin">
  <ul id="tabs_filter" class="tabs tabs-transparent center materialize-red darken-1">
    <li class="tab"><a class="active waves-effect" href="#all">All</a></li>
    <li class="tab"><a href="#branding" class="waves-effect">branding</a></li>
    <!--                <li class="tab"><a href="#web" class="waves-effect">Website</a></li>-->
    <li class="tab"><a href="#shirts" class="waves-effect">shirts</a></li>
    <li class="tab"><a href="#artworks" class="waves-effect">Artworks</a></li>
    <li class="tab"><a href="#photos" class="waves-effect">photos</a></li>
  </ul>
</div>
<div class="pushpin-placeholder"></div>

</nav>
<main>
  <div class="container">
    <div class="row add-pad-top grid">
      <div class="grid-sizer"></div>


      <?php
      
      $data_port = array(
        array("Neng's Delicacies Logo","branding","nengs/Nengs3.png"),
        array("Neng's White Logo Mockup","branding","nengs/Nengs1.png"),
        array("Neng's Delicacies Calling Card","branding","nengs/Nengs2.png"),
        array("Neng's Delicacies Old Logo","branding","nengs/Nengs4.png"),
        array("Drop Tower","photos","photo/Photo1.png"),
        array("Khile's Bubble Gun","photos","photo/Photo2.png"),
        array("Gundam","photos","photo/buster-4.jpg"),
        array("Kshatriya","photos","photo/kshatriya_cover.jpg"),
        array("Skycruiser","photos","photo/Photo5.png"),
        array("Circle Lovers","photos","photo/Photo6.png"),
        array("Gundam Unicorn","photos","photo/unicorn_fullarmor.jpg"),
        array("Kshatriya 2","photos","photo/kshatriya.jpg"),
        array("Bears","photos","photo/Photo9.png"),
        array("Ferris Wheel","photos","photo/Photo10.png"),
        array("Fire Exit","photos","photo/Photo11.png"),
        array("McDonald's Peanut Collection","photos","photo/Photo12.png"),
        array("Build Yourself","artworks","artworks/build-yourself.png"),
        array("Before I Do","artworks","artworks/before-i-do.jpg"),
        array("Tiwala Lang","artworks","artworks/tiwala-lang.png"),
        array("Acoustic Session","artworks","artworks/acoustic-session.png"),
        array("A Wonderful Morning","artworks","artworks/wonderful-morning.png"),
        array("A Great Evening","artworks","artworks/evening.png"),
        array("Stepping Stone","artworks","artworks/SteppingStone.png"),
        array("Area 6 Baesa Jersey","shirts branding","artworks/Area6_Jersey_Mockup.png"),
        array("Song Writing Seminar","artworks","artworks/song_writing_poster.png"),
        array("Unity Games Tarpaulin Design","artworks","artworks/UnityGamesTarp.png"),
        array("Unity Games Organizer Shirt Design","shirts artworks","artworks/organizers-mockup.png"),
        array("The Daily Opium","branding","artworks/daily-opium.png"),
        array("Focus Ahead","shirts","shirts/focus_mockup.png"),
        array("You Don't Make Me Who I Am","shirts","shirts/Shirt2.png"),
        array("Aries","shirts","shirts/aries.png"),
        array("Pokemon Go! Team Instinct","shirts","shirts/instinct_yellow2.png"),
        array("Coffee Cat","shirts","shirts/Shirt5.png"),
        array("Hopes Up High","shirts","shirts/Shirt6.png"),
      );
      
      shuffle($data_port);
      
      for ($i = 0 ; $i < count($data_port) ; $i++ ):
      
      ?>


        <div class="col s6 m4 <?php echo $data_port[$i][1]; ?> <?php if($i>6) echo 'inactive'; ?>">
          <div class="card hoverable">
            <div class="card-image">
              <img src="assets/img/<?php echo $data_port[$i][2] ?>" alt="<?php echo $data_port[$i][0] ?>" title="<?php echo $data_port[$i][0] ?>" class="responsive-img">
            </div>
            <div class="card-content">
              <p>
                <?php echo $data_port[$i][0] ?>
              </p>
            </div>
          </div>
        </div>

        <?php endfor ?>


    </div>
  </div>
</main>
