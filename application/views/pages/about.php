<main>
  <div class="container">
    <div class="row add-pad-top grid">
      <div class="grid-sizer"></div>
      <div class="col s12 m6">
        <div class="card white">
          <div class="card-content">
            <span class="card-title">Design Tools</span>
            <p>
              <span class="chip large blue lighten-3">
                                    <img src="assets/img/portfolio/icons/PS.svg" alt="" class="circle">
                                    Photoshop
                                </span>
              <span class="chip large amber lighten-3">
                                    <img src="assets/img/portfolio/icons/AI.svg" alt="" class="circle">
                                    Illustrator
                                </span>
              <span class="chip large green lighten-3">
                                    <img src="assets/img/portfolio/icons/Muse.svg" alt="" class="circle">
                                    Muse
                                </span>
              <span class="chip large purple lighten-4">
                                    <img src="assets/img/portfolio/icons/AE.svg" alt="" class="circle">
                                    After Effects
                                </span>
              <span class="chip large deep-purple lighten-4">
                                    <img src="assets/img/portfolio/icons/XD.svg" alt="" class="circle">
                                    Xperience Design
                                </span>
            </p>
          </div>
        </div>

        <div class="card">
          <div class="card-content">
            <span class="card-title">Web Development</span>

            <p>
              <span class="chip large orange lighten-3">
                                    <img src="assets/img/portfolio/icons/html5-logo.svg" alt="">
                                    HTML5
                                </span>
              <span class="chip large light-blue lighten-3">
                                    <img src="assets/img/portfolio/icons/css3-logo.svg" alt="">
                                    CSS3
                                </span>
              <span class="chip large yellow lighten-3">
                                    <img src="assets/img/portfolio/icons/js-logo.svg" alt="">
                                    JavaScript
                                </span>
              <span class="chip large indigo lighten-3">
                                    <img src="assets/img/portfolio/icons/php-logo.svg" alt="">
                                    PHP
                                </span>
              <span class="chip large purple lighten-3">
                                    <img src="assets/img/portfolio/icons/bootstrap-logo.svg" alt="">
                                    Bootstrap
                                </span>
              <span class="chip large materialize-red lighten-4">
                                    <img src="assets/img/portfolio/icons/materializecss-logo.svg" alt="">
                                    MaterializeCSS
                                </span>
            </p>
          </div>
        </div>


      </div>
      <div class="col s12 m6">
        <div class="card">
          <div class="card-image">
            <img src="assets/img/portfolio/employment-bg.svg" alt="">
            <span class="card-title grey-text text-darken-4">
                            Employment
                            </span>
          </div>
          <div class="card-content">
            <div class="collection">
              <div class="collection-item avatar">
                <img src="assets/img/portfolio/icons/bigbox-logo.png" alt="" class="circle z-depth-2">
                <span class="title">Front-end Web Developer &amp; Graphic Designer</span>
                <p>
                  Bigbox,<span class="grey-text darken-2"> Feb 2017 to Nov 2017</span>
                </p>
              </div>
              <div class="collection-item avatar">
                <img src="assets/img/portfolio/icons/affinity-x-logo.png" alt="" class="circle z-depth-2">
                <span class="title">Senior Web Designer</span>
                <p>
                  Affinity X,<span class="grey-text darken-2"> Dec 2014 to Mar 2016</span>
                </p>
              </div>
              <div class="collection-item avatar">
                <img src="assets/img/portfolio/icons/hibu-logo.png" alt="" class="circle z-depth-2">
                <span class="title">Web Designer</span>
                <p>
                  Hibu,<span class="grey-text darken-2"> - Jul 2013 to Nov 2014</span>
                </p>
              </div>
              <div class="collection-item avatar">
                <img src="assets/img/portfolio/icons/psi-logo.png" alt="" class="circle z-depth-2">
                <span class="title">Programmer</span>
                <p>
                  Pinoy Servitek Inc.,<span class="grey-text darken-2"> Mar to Apr 2011</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col s12 m6">
        <div class="card">
          <div class="card-image">
            <img src="assets/img/portfolio/education-bg.svg" alt="">
            <span class="card-title grey-text text-darken-4">
                            Education
                            </span>
          </div>
          <div class="card-content">
            <div class="collection">
              <div class="collection-item avatar">
                <img src="assets/img/portfolio/icons/sti-logo.png" alt="" class="circle z-depth-2">
                <span class="title">BS Computer Science</span>
                <p>
                  STI Caloocan,<span class="grey-text darken-2"> 2012 - 2013</span>
                </p>
              </div>
              <div class="collection-item avatar">
                <img src="assets/img/portfolio/icons/qcpu-logo.png" alt="" class="circle z-depth-2">
                <span class="title">Computer Programming</span>
                <p>
                  QCPU,<span class="grey-text darken-2"> 2010 - 2011</span>
                </p>
              </div>
              <div class="collection-item avatar">
                <img src="assets/img/portfolio/icons/nehs-logo.png" alt="" class="circle z-depth-2">
                <span class="title">New Era High School</span>
                <p>
                  <span class="grey-text darken-2">S.Y. 2005 - 2009</span>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col m6">
        <div class="card">
          <div class="card-content">asdf</div>
        </div>
      </div>
    </div>
  </div>
</main>
