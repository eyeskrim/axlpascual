<!DOCTYPE html>
<html>


<head>
  <!--Import Google Icon Font-->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <!--Import materialize.css-->
  <link type="text/css" rel="stylesheet" href="assets/css/materialize.min.css" media="screen,projection" />
  <link type="text/css" rel="stylesheet" href="assets/css/helper.css" />
  <link type="text/css" rel="stylesheet" href="assets/css/template.css" />

  <!--Let browser know website is optimized for mobile-->
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />

  <title>Axle Pascual</title>
</head>


<body>

  <!-- Side Nav -->
  <ul class="side-nav" id="side_nav">
    <li>
      <a href="home" class="active">
				<i class="material-icons">dashboard</i>
				Portfolio
			</a>
    </li>
    <li>
      <a href="about">
				<i class="material-icons">person</i>
				About Me
			</a>
    </li>
  </ul>


  <!-- App Bar -->
  <nav class="materialize-red appbar nav-extended">
    <div class="nav-wrapper">
      <a href="#" data-activates="side_nav" class="button-collapse"><i class="material-icons">menu</i></a>

      <ul class="left">
        <li><a href="#" class="brand-logo waves-effect add-pad-left add-pad-right">Axle Pascual</a></li>
      </ul>
      <ul id="nav-mobile" class="right hide-on-med-and-down">
        <li>
          <a href="home" class="active waves-effect">
              <i class="material-icons left">dashboard</i>
              Portfolio
          </a>
        </li>
        <li>
          <a href="about" class="waves-effect">
              <i class="material-icons left">person</i>
              About Me
          </a>
        </li>
      </ul>
    </div>
    <p></p>
    <h2 class="center thin no-margin-bot add-margin-top-32">Let's make it simple</h2>
    <p class="center no-margin-top add-margin-bot">Simple. Fast. Focused.</p>
