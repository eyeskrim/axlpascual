</body>
<footer class="page-footer grey lighten-2">
  <div class="container">
    <div class="row">
      <div class="col l6 s12 grey-text text-darken-2">
        <h5>Footer Content</h5>
        <p class="grey-text text-darken-1">You can use rows and columns here to organize your footer content.</p>
      </div>
      <div class="col l4 offset-l2 s12">
        <h5 class="grey-text text-darken-2">Links</h5>
        <ul>
          <li><a class="grey-text text-darken-1" href="#!">Link 1</a></li>
          <li><a class="grey-text text-darken-1" href="#!">Link 2</a></li>
          <li><a class="grey-text text-darken-1" href="#!">Link 3</a></li>
          <li><a class="grey-text text-darken-1" href="#!">Link 4</a></li>
        </ul>
      </div>
    </div>
  </div>
  <div class="footer-copyright grey-text text-darken-2">
    <div class="container">
      © 2017 Copyright. All Rights Reserved
      <a class="grey-text text-darken-1 right" href="#!">Axle Pascual</a>
    </div>
  </div>
</footer>

<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="assets/js/materialize.js"></script>
<script type="text/javascript" src="assets/js/masonry.pkgd.min.js"></script>
<script type="text/javascript" src="assets/js/template.js"></script>

</html>
